package com.example.daggerpractice.dagger_injection;

import com.example.daggerpractice.dagger_injection.auth.AuthModule;
import com.example.daggerpractice.dagger_injection.auth.AuthViewModelsModule;
import com.example.daggerpractice.dagger_injection.main.MainFragmentBuildersModule;
import com.example.daggerpractice.dagger_injection.main.MainModule;
import com.example.daggerpractice.dagger_injection.main.MainViewModelsModule;
import com.example.daggerpractice.ui.auth.AuthActivity;
import com.example.daggerpractice.ui.main.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.ContributesAndroidInjector;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module
public abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(
            modules = {AuthViewModelsModule.class, AuthModule.class})
    abstract AuthActivity contributeAuthActivity();

    @ContributesAndroidInjector(
        modules = {MainFragmentBuildersModule.class, MainViewModelsModule.class, MainModule.class}
    )
    abstract MainActivity contributeMainActivity();

}

package com.example.daggerpractice.dagger_injection;

import androidx.lifecycle.ViewModelProvider;

import com.example.daggerpractice.viewmodels.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelFactory);

//    @Provides
//    static ViewModelProvider.Factory bindFactory(ViewModelProviderFactory factory) {   //This is a method same the same like upper method with difference, that it will realize in extended class
//
//        //Body of code
//
//        return factory;
//    }
}
